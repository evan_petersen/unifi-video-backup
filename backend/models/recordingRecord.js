'use strict';

/*

This houses the mongoose schema for recordings

*/

var {mongoose} = require('../config');
var mongoosePaginate = require('mongoose-paginate-v2');

var schema = mongoose.Schema({ 
  _id: String,
  startTime: Date,
  endTime: Date,
  cameraName: String,
  eventType: String,
  uploaded: {
    type: Boolean,
    default: false
  },
  processing: {
    type: Boolean,
    default: false,
    expires: 60*60
  },
  upload: {
    Location: String,
    Bucket: String,
    Key: String,
    ETag: String,
    DownloadUrl: {
      type: String,
      expires: 60*30
    },
    LastModified: Date,
    Size: Number,
    StorageClass: String,
    LastSeen: Date,
    UUID: String,
    Lost: {
      type: Boolean,
      default: false,
    },
  }
});

schema.plugin(mongoosePaginate);

var recordingRecord = mongoose.model('recordingRecord', schema);

module.exports = recordingRecord;