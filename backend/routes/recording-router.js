'use strict';

const express = require('express')

const RecordingCtrl = require('../controllers/recording-ctrl')

const router = express.Router()

router.get('/recording/download/:id', RecordingCtrl.getRecordingDownloadById)
router.get('/recording/:id', RecordingCtrl.getRecordingById)
router.get('/recordings/processed', RecordingCtrl.getProcessedRecordings)
router.get('/recordings/queued', RecordingCtrl.getQueuedRecordings)
router.get('/recordings', RecordingCtrl.getRecordings)
router.get('/cameras', RecordingCtrl.getCameras)


module.exports = router;