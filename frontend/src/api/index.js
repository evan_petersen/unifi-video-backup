import axios from 'axios'

const api = axios.create({
    baseURL: 'http://' + window.location.hostname + ':49160/api',
})

export const getAllRecordings = parameters => api.get(`/recordings`, { params: { parameters } })
export const getProcessedRecordings = parameters => api.get(`/recordings/processed`, { params: { parameters }})
export const getQueuedRecordings = () => api.get(`/recordings/queued`)
export const getRecordingById = id => api.get(`/recording/${id}`)
export const getRecordingDownloadById = id => api.get(`/recording/download/${id}`)
export const getAllCameras = () => api.get('/cameras')

const apis = {
    getAllRecordings,
    getRecordingById,
    getRecordingDownloadById,
    getProcessedRecordings,
    getQueuedRecordings,
    getAllCameras
}

export default apis