'use strict';

/*

This is used to manage s3 operations.  

TODO: Move all s3 ops into this file

*/

var {s3, globals} = require('../config');

const signedUrlExpireSeconds = 60*30; // 30 minutes

var generateDownloadLinkForRecordingRecord = async function(recordingRecord) {

  var url;

  console.log('Recording Record for download:' + recordingRecord._id);

  if (!recordingRecord) return false;

  try {

    url = await s3.getSignedUrl('getObject', {
      Bucket: process.env.bucket,
      Key: recordingRecord.upload.Key,
      Expires: signedUrlExpireSeconds
    });

    recordingRecord.upload.DownloadUrl = url;
  
    recordingRecord.save();

  } catch (error) {
    console.log(error);
    return false;
  }
  
  return url;

}

var getAllObjects = async function(startingToken = null, maxKeys = null) {

  let params = {Bucket: globals.s3.bucket};

  if (typeof maxKeys !== null) params.MaxKeys = maxKeys;

  if (typeof startingToken !== null) params.ContinuationToken = startingToken;

  let objects = await s3.listObjectsV2(params).promise();

  return objects;

}

module.exports = {
  generateDownloadLinkForRecordingRecord,
  getAllObjects
}