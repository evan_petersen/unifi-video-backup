'use strict';

var {getAllObjects} = require('../models/s3Operations');
var recordingRecord = require('../models/recordingRecord');
var { v4 } = require('uuid');



async function main() {


  // Retreive all objects from s3 (1,000 at a time)
  //
  // Match objects in Mongoose based on ETag
  //
  // Update objects in Mongoose with new info and create UUID
  //
  // Set all objects in Mongoose that were not found in Mongo to possibly missing in s3

  const lastSeen = new Date();
  const UUID = v4();
  let continuationToken = null;

  do {

    let s3objects = await getAllObjects(continuationToken);

    for (let s3object in s3objects.Contents) {

      try {
        await recordingRecord.findOneAndUpdate(
          {
            'upload.ETag': s3objects.Contents[s3object].ETag
          }, {
            'upload.LastModified': s3objects.Contents[s3object].LastModified,
            'upload.Size': s3objects.Contents[s3object].Size,
            'upload.StorageClass': s3objects.Contents[s3object].StorageClass,
            'upload.LastSeen': lastSeen,
            'upload.Key': s3objects.Contents[s3object].Key,
            'upload.UUID': UUID,
            'upload.Lost': false
          }
        )
      } catch (err) {
        console.log(err);
        process.send('Error running reconcileAws.js: ');
        process.send(err);
      }
    }

    if (s3objects.IsTruncated) {
      continuationToken = s3objects.NextContinuationToken;
    } else {
      continuationToken = null;
    }

  } while (continuationToken)

  //Now look for any objects that have been uploaded but do not have the most recent UUID
  //And set them to lost

  await recordingRecord.updateMany(
    { 
      'upload.UUID': { $ne: UUID },
      'uploaded': true
    },
    { 
      "$set": { 'upload.Lost': true } 
    }
  )

  process.send('Sleeping for 24 hours');

  setTimeout(main, 1000*60*60*24);
}

process.send('reconcileAws starting up');
main();