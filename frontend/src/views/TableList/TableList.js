import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Grid from '@material-ui/core/Grid';
import ReactMoment from 'react-moment';
import apis from 'api';
import Skeleton from '@material-ui/lab/Skeleton';
import MomentUtils from "@date-io/moment";
import Select from '@material-ui/core/Select';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControl from '@material-ui/core/FormControl';
import { v4 } from 'uuid';
import "date-fns";
import {
  MuiPickersUtilsProvider,
  TimePicker,
  DateTimePicker,
  DatePicker
} from "@material-ui/pickers";

const useStyles1 = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  function handleFirstPageButtonClick(event) {
    onChangePage(event, 0);
  }

  function handleBackButtonClick(event) {
    onChangePage(event, page - 1);
  }

  function handleNextButtonClick(event) {
    onChangePage(event, page + 1);
  }

  function handleLastPageButtonClick(event) {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  }

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
    '&:hover': {
      backgroundColor: '#ccc',
    },
  },
}))(TableRow);

const useStyles2 = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  formControl: {
    margin: theme.spacing(0),
    minWidth: 120,
    marginBottom: theme.spacing(1),
  },
}));

export default function CustomPaginationActionsTable() {
  var tempDate = new Date();
  var today = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate());

  const classes = useStyles2();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(50);
  const [rows, setRows] = React.useState([]);
  const [loaded, setLoaded] = React.useState(false);
  const [totalDocs, setTotalDocs] = React.useState(0);

  //const [fromFilterDate, setFromFilterDate] = React.useState(new Date(today.getTime()));
  //const [toFilterDate, setToFilterDate] = React.useState(new Date(today.getTime()));
  const [fromFilterDate, setFromFilterDate] = React.useState(null);
  const [toFilterDate, setToFilterDate] = React.useState(null);
  const [uploaded, setUploaded] = React.useState('');
  const [camera, setCamera] = React.useState('');
  const [cameras, setCameras] = React.useState([]);
  const [filterEnabled, setFilterEnabled] = React.useState(false);
  const [forceUpdate, setForceUpdate] = React.useState(v4());

  function handleFilterChange() {
    setPage(0);
    setFilterEnabled(!filterEnabled); 
  }

  const handleUploadedChange = event => {
    setUploaded(event.target.value);
    if (filterEnabled) {
      setPage(0);
      forceEffectUpdate();
    }
  };

  const handleCameraChange = event => {
    setCamera(event.target.value);
    if (filterEnabled) {
      setPage(0);
      forceEffectUpdate();
    }
  };

  function forceEffectUpdate(){
    setForceUpdate(v4());
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
    forceEffectUpdate();
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
    forceEffectUpdate();
  }

  function handleSetFromDate(value) {
    if (value === null) {
      setFromFilterDate(null);
    } else {
      let tempDate = value.toDate();
      tempDate.setHours(0,0,0,0);
      setFromFilterDate(tempDate);
    }

    if(filterEnabled) {
      setPage(0);
      forceEffectUpdate();
    }
  }

  function handleSetToDate(value) {
    if (value === null) {
      setToFilterDate(null);
    } else {
      let tempDate = value.toDate();
      tempDate.setHours(0,0,0,0);
      setToFilterDate(tempDate);
    }

    if (filterEnabled) {
      setPage(0);
      forceEffectUpdate();
    }
  }

  function download(id) {
    apis.getRecordingDownloadById(id).then(response => {
      window.open(response.data.data, "_blank");
    });
  }

  React.useEffect(() => {
    let mounted = true;

    apis.getAllCameras().then(response => {
      setCameras(response.data.data);
    });

    return () => {
      mounted = false;
    };

  }, []);

  React.useEffect(() => {
    
    // eslint-disable-next-line
    let mounted = true;

    setLoaded(false);
    setRows([]);
    setTotalDocs(0);

    let filter = {page: page + 1, limit: rowsPerPage};

    if (filterEnabled) {
      //Add filter information
      if (camera !== '') {
        filter.camera = camera;
      }

      if (uploaded !== '') {
        filter.uploaded = uploaded;
      }

      if (fromFilterDate !== null) filter.fromDate = fromFilterDate;
      if (toFilterDate !== null) filter.toDate = toFilterDate;

      if ((fromFilterDate !== null) && (toFilterDate !== null)) {
        if (fromFilterDate.getTime() > toFilterDate.getTime()) {
          let tempFromFilter = fromFilterDate;
          setFromFilterDate(toFilterDate);
          filter.fromDate = toFilterDate;

          setToFilterDate(tempFromFilter);
          filter.toDate = tempFromFilter;  
        }

        if (fromFilterDate.getTime() === toFilterDate.getTime()) {
          let tempNewDate = new Date(toFilterDate.setDate(toFilterDate.getDate() +1));
          setToFilterDate(tempNewDate);
          filter.toDate = tempNewDate;
        }
      }
    }

    const fetchRows = () => {

      apis.getProcessedRecordings(filter).then(response => {
        setRows(response.data.data);
        setTotalDocs(response.data.count);     
      }).catch(function (error) {
        if (error.response) {

          if(error.response.status == '404') {
            alert("No recordings found");
          } else {
            alert('Unknown Error Occurred.  Check console log.');
          }
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
        console.log(error);
      }).finally(function() {
        setLoaded(true);
      });

    }

    fetchRows();

    return () => {

      mounted = false;
    };

  }, [filterEnabled, forceUpdate]);

  return (
    <GridContainer>
      <GridItem xs={12} lg={8}>
        <Paper className={classes.root}>
          <div className={classes.tableWrapper}>
            <Table className={classes.table} size="small" >
              <TableHead>
                <TableRow>
                  <StyledTableCell align="left">
                    Camera Name
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    Start Time
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    Duration
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    Download
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell colSpan="4">
                  <ExpansionPanel>
                    <ExpansionPanelSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography className={classes.heading}>Filter Recordings</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <MuiPickersUtilsProvider utils={MomentUtils}>

                        <Grid container>
                        
                        <Grid item xs={6} lg={3}>
                        <FormControl className={classes.formControl}>
                          <InputLabel id="select-uploaded-label">Uploaded</InputLabel>
                          <Select
                            labelId="select-uploaded-label"
                            id="select-uploaded"
                            value={uploaded}
                            onChange={handleUploadedChange}
                          >
                              <MenuItem value=''>Any</MenuItem>
                              <MenuItem value={true}>True</MenuItem>
                              <MenuItem value={false}>False</MenuItem>
                          </Select>
                        </FormControl>
                          </Grid>

                          <Grid item xs={6} lg={3}>
                          <FormControl className={classes.formControl}>
                          <InputLabel id="select-camera-label">Camera</InputLabel>
                          <Select
                            labelId="select-camera-label"
                            id="select-camera"
                            autoWidth={true}
                            value={camera}
                            onChange={handleCameraChange}
                          >
                            <MenuItem value=''>Any</MenuItem>
                            {cameras.map((cameraItem, index) => (
                            
                              <MenuItem value={cameraItem} key={index}>{cameraItem}</MenuItem>
                            
                            ))}
                          </Select>
                          </FormControl>
                          </Grid>

                          <Grid item xs={12} lg={3}>
                          <FormControl className={classes.formControl}>
                          <DatePicker
                            label="From Date"
                            value={fromFilterDate}
                            //inputVariant="outlined"
                            onChange={handleSetFromDate}
                            clearable={true}
                          />       
                          </FormControl>
                          </Grid>
                          <Grid item xs={12} lg={3}>
                          <FormControl className={classes.formControl}>
                          <DatePicker
                            label="To Date"
                            //inputVariant="outlined"
                            value={toFilterDate}
                            onChange={handleSetToDate}
                          />  
                          </FormControl>
                          </Grid>
                          
                            <Grid item xs={12} lg={3}>
                              {!filterEnabled && 
                                <Button variant="contained" color="primary" onClick={handleFilterChange}>Filter</Button>
                              }

                              {filterEnabled && 
                                <Button variant="contained" color="primary" onClick={handleFilterChange}>Disable Filter</Button>
                              }
                            </Grid>
                          </Grid>
                      </MuiPickersUtilsProvider>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[50, 100, 1000]}
                    colSpan={4}
                    count={totalDocs}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: { 'aria-label': 'Rows' },
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
                {!loaded && 
                  <StyledTableRow>    
                    <StyledTableCell align="left">
                      <Skeleton variant="text" />
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Skeleton variant="text" />
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Skeleton variant="text" />
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Skeleton variant="rect" width={48} height={48} />
                    </StyledTableCell>
                  </StyledTableRow>
                }

                {rows.map(row => (
                  <StyledTableRow key={row._id}>    
                    <StyledTableCell align="left">
                      {row.cameraName}
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <ReactMoment format="YYYY/MM/DD HH:mm">
                        {row.startTime}
                      </ReactMoment>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <ReactMoment duration={row.startTime} date={row.endTime} />
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      {row.uploaded && 
                        <IconButton size='small' onClick={() => download(row._id)}  >
                          <CloudDownloadIcon />
                        </IconButton>
                      }
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[50, 100, 1000]}
                    colSpan={4}
                    count={totalDocs}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: { 'aria-label': 'Rows' },
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </div>
        </Paper>
      </GridItem>
    </GridContainer>
  );
}