# Unifi Video Backup

Unifi Video Backup automatically creates off-site backups of Unifi Video recordings using S3.

This application is in its infancy and has not been thoroughly tested.  Do not depend on this application.  Community help is requested to improve Unifi Video Backup.

## Prerequisites

The following are prerequisites to using this application:

1. Unifi Video Server with cameras set up to record
2. AWS Account with an S3 bucket and an IAM user with S3 permissions for that bucket
3. Docker

At the time of writing, Unifi Video Server is on version 3.10.10.  This application relies on Unifi Video Server's undocumented API which may change in future releases.

## Installation and Setup

1. Generate Unifi Video Server API key. To generate an API key, log in to your Unifi Video Server, click USERS (bottom left corner), add user, and grant API access.  Take note of the generated **unifiApiKey**.

2. Create an AWS **bucket** and AWS IAM user with full S3 permission for said bucket. Take note of the provided **accessKeyId** and **secretAccessKey**

3. Download the repo onto the computer or server that you plan on housing the application. Make sure Docker is installed, the computer has network access to your Unifi Video Server, and can reach the internet. Run `git clone git@bitbucket.org:evan_petersen/unifi-video-backup.git` in the directory you wish to house the source code.  Copy docker-compose.yml.example to docker-compose.yml and open the new file with your editor.  You will need to adjust the environment variables with the generated items above and provide a path for mongoDb to store its documents.  All items with <> must be updated or the application will not work.

4. Run `docker-compose up` in the directory that you downloaded the repository.

## Usage ##

The first run will take some time to start as the server image has to be built.  As soon as the server image is built, the application will start looking for recordings in your Unifi Video Server.  Once found, those videos will be uploaded to yor S3 bucket.

There is a frontend with limited functionality that shows which recordings have been uploaded with an option to download, and which recordings have been discovered in you Unifi Video Server but not yet uploaded.  To access the frontend go to **<ip of docker container>:3000**

You can stop the application at any time with `docker-compose stop`

