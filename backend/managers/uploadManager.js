'use strict';

/*

uploadManager.js will look for unprocessed recording records in
MongoDB and create workers to process them.

*/

const cluster = require('cluster');
var moment = require('moment');
const got = require('got');
const {promisify} = require('util');
const stream = require('stream');
const pipeline = promisify(stream.pipeline);
var recordingRecordModel = require('../models/recordingRecord');
var {s3} = require('../config');

const unifiApiKey = process.env.unifiApiKey;
const unifiServerUrl = process.env.unifiServerUrl;

var numWorkers = 2;

async function markRecordingProcessed(recordingId, data) {

  var result;

  try {
    result = await recordingRecordModel.updateOne(
      {_id: recordingId},
      {
        'uploaded': true,
        'processing': false,
        'upload.Location': data.Location,
        'upload.Bucket': data.Bucket,
        'upload.Key': data.Key,
        'upload.ETag': data.ETag
      }
    );

  } catch (err) {
    process.send(err);

  } finally {
    return result;
  }
}

function writeToS3(Key, Bucket, recordingId) {
  const Body = new stream.PassThrough();

  s3.upload({
    Body: Body,
    Key: Key,
    Bucket: Bucket,
    Metadata: {
      'x-amz-meta-unifi-id': recordingId
    }
  },{partSize: 25 * 1024 * 1024, queueSize: 50})
   .on('httpUploadProgress', progress => {
       //console.log('progress', progress);
   })
   .send((err, data) => {
     if (err) {
       Body.destroy(err);
       process.send(err);
     } else {
       process.send(`File uploaded and available at ${data.Location}`);

       markRecordingProcessed(recordingId, data);
       Body.destroy();
     }
  });

  return Body;
}

async function getRecordingToProcess() {

  var result;

  try {

    result = await recordingRecordModel.findOneAndUpdate(
      {
        uploaded: false,
        processing: {
          $ne: true
        }
      },
      {'processing': true}
    );

  } catch (err) {
    process.send('Get recording to process error');
    process.send(err);

  } finally {
    if (result == null) {
      process.send('No new recordings to upload');
      return false;
    }
  }

  return result;
}

async function resetProcessing() {

  var result;

  try {
    result = await recordingRecordModel.updateMany(
      {processing: true},
      {processing: false}
    );

  } catch (err) {
    process.send(err);

  } finally {
    return result;
  }

}

async function main() {

  let result = await getRecordingToProcess();

  if (result == false) {
    process.send('Nothing left to process, sleeping 10 minutes by ' + process.pid);
    setTimeout(main, 1000*60*10);
    return;
  }

  process.send('Processing: ' + result._id + ' by ' + process.pid);

  let uri = 'https://' + unifiServerUrl + '/api/2.0/recording/' + result._id + '/download';
  let searchParams = new URLSearchParams([['apiKey', unifiApiKey]]);
  let bucket = process.env.bucket;

  let pathDate = moment(result.startTime).format('YYYYMMDD');

  let keyStartDate = moment(result.startTime).format('YYYYMMDD.HHmmss');
  let keyEndDate = moment(result.endTime).format('YYYYMMDD.HHmmss');

  let key = result.cameraName + '/' + pathDate + '/' + keyStartDate + '-' + keyEndDate + '.' + result.cameraName + '.mp4';

  try {
    await pipeline(
      got.stream(uri,{searchParams, rejectUnauthorized: false}),
      writeToS3(key, bucket, result._id)
    );
  } catch (err) {
    process.send('Pipeline error by ' + process.pid);
    process.send(err);
  } finally {

  }

  process.send('Processed: ' + result._id + ' by ' + process.pid);
  main();
}


process.on('message', (msg) => {
  //process.send(''); send message back
});

if (cluster.isMaster) {
  process.send(`Master ${process.pid} is running`);

  resetProcessing();

  // Fork workers.
  for (let i = 0; i < numWorkers; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    process.send(`worker ${worker.process.pid} died`);
  });

  for (const id in cluster.workers) {
    cluster.workers[id].on('message', (m) => {
      process.send('Worker: ' + m);
    });
  }

  setInterval(() => {
      process.send('upload manager still alive!');
  }, 1000*60*10);
} else {
  process.send(`Worker ${process.pid} started`);
  main();
}
