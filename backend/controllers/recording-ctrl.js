'use strict';

const recordingRecord = require('../models/recordingRecord');
const s3Operations = require('../models/s3Operations');

var getRecordingById = async (req, res) => {
  await recordingRecord.findOne({ _id: req.params.id }, (err, recording) => {
      if (err) {
          return res.status(400).json({ success: false, error: err })
      }

      if (!recording) {
          return res
              .status(404)
              .json({ success: false, error: `Recording not found` })
      }
      return res.status(200).json({ success: true, data: recording })
  }).catch(err => console.log(err))
}

var getCameras = async (req, res) => {
  await recordingRecord.distinct( 'cameraName', (err, cameras) => {
      if (err) {
          return res.status(400).json({ success: false, error: err })
      }

      if (!cameras) {
          return res
              .status(404)
              .json({ success: false, error: `Cameras not found` })
      }
      return res.status(200).json({ success: true, data: cameras })
  }).catch(err => console.log(err))
}

var getRecordings = async (req, res) => {

  await recordingRecord.find(find, null, {sort: {startTime: -1}}, (err, recording) => {
      if (err) {
          return res.status(400).json({ success: false, error: err })
      }
      if (!recording.length) {
          return res
              .status(404)
              .json({ success: false, error: `Recording not found` })
      }
      return res.status(200).json({ success: true, data: recording })
  }).catch(err => console.log(err))
}

var getProcessedRecordings = async (req, res) => {

  //TODO: Make this generic and serve queued recordings as well through get params
  let requestParams = JSON.parse(req.query.parameters);

  let query = { }; //Filter object for Mongoose  Put back uploaded: true 
  let options = { sort: {startTime: -1} }; //Pagination object for Mongoose-Pagination

  if (typeof requestParams.camera !== "undefined") {
    query.camera = String.valueOf(requestParams.camera);
  }

  if (typeof requestParams.uploaded !== "undefined") {
    query.uploaded = requestParams.uploaded;
  }

  if (typeof requestParams.fromDate !== "undefined") {
    query.startTime = {
      $gte: requestParams.fromDate
    }
  }

  if (typeof requestParams.toDate !== "undefined") {
    if (typeof query.startTime !== "undefined") {
      query.startTime.$lte = requestParams.toDate;
    } else {
      query.startTime = {
        $lte: requestParams.toDate
      }
    }
  }

  if (typeof requestParams.page !== "undefined") {
    options.page = parseInt(requestParams.page, 10);
  }

  if (typeof requestParams.limit !== "undefined") {
    options.limit = parseInt(requestParams.limit, 10);
  }

  await recordingRecord.paginate(query, options, (err, recording) => {

      if (err) {
          return res.status(400).json({ success: false, error: err })
      }
      if (!recording.docs.length) {
          return res
              .status(404)
              .json({ success: false, error: `Recording not found` })
      }
      return res.status(200).json({ success: true, data: recording.docs, totalPages: recording.totalPages, count: recording.totalDocs})
  }).catch(err => console.log(err))
}

var getQueuedRecordings = async (req, res) => {

  await recordingRecord.find({ uploaded: false }, null, {sort: {startTime: 1}}, (err, recording) => {
      if (err) {
          return res.status(400).json({ success: false, error: err })
      }
      if (!recording.length) {
          return res
              .status(404)
              .json({ success: false, error: `Recording not found` })
      }
      return res.status(200).json({ success: true, data: recording })
  }).catch(err => console.log(err))
}

var getRecordingDownloadById = async (req, res) => {

  var url;

  try {
    let recording = await recordingRecord.findOne({ _id: req.params.id });

    if (!recording) return res.status(400).json({ success: false, error: null });

    url = await s3Operations.generateDownloadLinkForRecordingRecord(recording);

    if (!url) return res.status(400).json({ success: false, error: null });

  } catch (err) {
    console.log(err);
    return res.status(400).json({ success: false, error: null });
  } 

  return res.status(200).json({ success: true, data: url });

}

module.exports = {
  getRecordings,
  getRecordingById,
  getRecordingDownloadById,
  getProcessedRecordings,
  getQueuedRecordings,
  getCameras
}