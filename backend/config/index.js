/*

Global Config Options

Anytime we need aws or mongoose, this file should be required.

*/

const mongoose = require('mongoose');
var AWS = require("aws-sdk");

const dbName = 'unifi-backup-db-test';
const mongooseUrl = 'mongodb://db:27017' + '/' + dbName;

mongoose.connect(mongooseUrl,  {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  serverSelectionTimeoutMS: 5000
});

var s3 = new AWS.S3({accessKeyId: process.env.accessKeyId, secretAccessKey: process.env.secretAccessKey});

var globals = {
  s3: { bucket: process.env.bucket },
}

module.exports = {
  mongoose,
  s3,
  globals
}