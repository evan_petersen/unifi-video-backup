import React, { Component } from "react";

// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";


import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { FixedSizeList } from 'react-window';
import AutoSizer from "react-virtualized-auto-sizer";

import { v4 } from 'uuid';

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

//const useStyles = makeStyles(styles);

class Dashboard extends Component {

  state = {
    messages: []
  };

  //classes = useStyles();

  componentDidMount() {
      const url = 'ws://' + window.location.hostname + ':3001';
      const connection = new WebSocket(url);

      connection.onopen = () => {
        console.log('WebSocket Client Connected');
      };
      connection.onmessage = (e) => {
        console.log(e.data);
        let messageObject = JSON.parse(e.data);
        this.setState(state => ({ messages: [{data: messageObject.message, id: v4(), timestamp: messageObject.timestamp}, ...state.messages] }));

      };
      connection.onerror = error => {
        console.log(`WebSocket error: ${error}`)
      }
  }

  render() {
    return (
    <div>
      <GridContainer>
      <GridItem xs={12}>
          <Card>
            <CardHeader color="success">
              <h4>Unifi Recording Backup</h4>
              <p>
                Automated Backups of Unifi Video Server to S3
              </p>
            </CardHeader>
            <CardBody>
              <List dense={true} style={{ background: "#272c34", color: "#ffffff" }}>
                {this.state.messages.map(stuff => (
                  <ListItem key={stuff.id} style={{ marginTop: '0px', marginBottom: '0px', paddingTop: '0px', paddingBottom: '0px' }}>
                    <ListItemText primary={stuff.timestamp} style={{ color: '#43a047', marginTop: '0px', marginBotom: '0px', paddingTop: '0px', paddingBottom: '0px', flexGrow: '0', paddingRight: '10px' }} /><ListItemText style={{ marginTop: '0px', marginBotom: '0px', paddingTop: '0px', paddingBottom: '0px' }} primary={stuff.data}/>
                  </ListItem>
                ))}
              </List>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
    )
  }
}

export default Dashboard