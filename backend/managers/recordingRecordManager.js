'use strict';

/*

recordingRecordManager is responsible for finding new records in
Unifi and adding them to mongo for processing

*/

const got = require('got');
var recordingRecordModel = require('../models/recordingRecord');

const unifiApiKey = process.env.unifiApiKey;
const unifiServerUrl = process.env.unifiServerUrl;

async function getRecordingRecordsFromUnifi(startTime = 0, endTime = 0) {

  let uri = 'https://' + unifiServerUrl + '/api/2.0/recording';

  let searchArray =[
    ['cause[]', 'fullTimeRecording'],
    ['idsOnly', 'false'],
    ['sortBy', 'startTime'],
    ['sort', 'asc'],
    ['apiKey', unifiApiKey]
  ];

  if (startTime > 0) searchArray.push(['startTime', startTime]);
  if (endTime > 0) searchArray.push(['endTime', endTime]);

  let searchParams = new URLSearchParams(searchArray);

  let response = await got(uri,{searchParams, rejectUnauthorized: false}).json();

  return response.data;

}

async function main() {
  process.send('Beginning recording process');
  let recordingRecords = await getRecordingRecordsFromUnifi();

  for (const recordingRecord of recordingRecords){

    if (recordingRecord.inProgress == true) continue;

    let isProcessed = await recordingRecordModel.exists({_id: recordingRecord._id});

    if (isProcessed == true) continue;

    try {
      let model = new recordingRecordModel(
        {
          _id: recordingRecord._id,
          startTime: recordingRecord.startTime,
          endTime: recordingRecord.endTime,
          cameraName: recordingRecord.meta.cameraName,
          eventType: recordingRecord.eventType,
        }
      );

      await model.save(function (err) {
        if (err) {
          console.log(err);
          process.send('model.save error: ');
          process.send(err);
        }
        // saved!
      });
  
    } catch (err) {
      process.send('model.save error');
      process.send(err);
      console.log(err);

    }
  }

  process.send('Sleeping for 10 minutes');

  setTimeout(main, 1000*60*10);
}

process.send('recordingRecordManager starting up');
main();
