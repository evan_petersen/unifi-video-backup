'use strict';

/*

This is the main entrypoint into the application and is
responsible for starting the two primary child processes,
upload manager and recordingRecordManager.

Additionally, the backend API runs off of this thread.

*/

const childProcess = require('child_process');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const recordingRouter = require('./routes/recording-router');
const Moment = require('moment')

const app = express();
const apiPort = 8080;

const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: 3001 })

wss.on('connection', ws => {
  ws.on('message', message => {
    console.log(`Received message => ${message}`)
  })

  sendMessageToAll('New client joined');
})

function sendMessageToAll(message) {
  wss.clients.forEach(function each(ws) {
    ws.send(JSON.stringify({
      timestamp: Moment().format("YYYY/MM/DD HH:mm:ss") + '# ',
      message: message
    }));
  });

}

setInterval(() => {
  sendMessageToAll('Log heartbeat');
}, 1000*60*1);

const uploadManager = childProcess.fork('backend/managers/uploadManager.js');

uploadManager.on('message', (m) => {
  console.log('uploadManager.js: ', m);
  sendMessageToAll('uploadManager.js: ' + JSON.stringify(m));
});

const recordingRecordManager = childProcess.fork('backend/managers/recordingRecordManager.js');

recordingRecordManager.on('message', (m) => {
  console.log('recordingRecordManager.js: ', m);
  sendMessageToAll('recordingRecordManager.js: ' + JSON.stringify(m));
});

const reconcileAws = childProcess.fork('backend/managers/reconcileAws.js');

reconcileAws.on('message', (m) => {
  console.log('reconcileAws.js: ', m);
  sendMessageToAll('reconcileAws.js: ' + JSON.stringify(m));
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hello World! API is location at /api/')
})

app.use('/api', recordingRouter)

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))

